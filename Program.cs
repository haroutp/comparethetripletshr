﻿using System;
using System.Collections.Generic;

namespace CompareTheTriplets
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> a = new List<int>{2, 5, 5};

            List<int> b = new List<int>{1, 4, 5};
            List<int> result = compareTriplets(a, b);
            foreach (var item in result)
            {
                System.Console.Write(item + " ");
            }
        }

        private static List<int> compareTriplets(List<int> a, List<int> b)
        {
            List<int> result = new List<int>();
            int aResult = 0;
            int bResult = 0;
            for (int i = 0; i < a.Count; i++)
            {
                if(a[i] != b[i]){
                    if(a[i] > b[i]){
                        aResult++;
                    }else{
                        bResult++;
                    }
                }
            }

            result.Add(aResult);
            result.Add(bResult);

            return result;
        }
    }
}
